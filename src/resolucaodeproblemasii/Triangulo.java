/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package resolucaodeproblemasii;

import java.util.ArrayList;

/**
 *
 * @author MatheusMorcinek
 */
public class Triangulo extends Figura{
    
    
    
    public Triangulo(int x0, int y0, int x1, int y1){
      this.nlados = 3; 
      Vertice v1 = new Vertice();
      Vertice v2 = new Vertice();
      Vertice v3 = new Vertice();
      
      v1.x = x0;
      v1.y = y0;
      v2.x = x1;
      v2.y = y1;
      this.tamanhoLado = x0 - x1;
      if(tamanhoLado<0){
          tamanhoLado = tamanhoLado*-1;
      }
      v3.x = x0+(tamanhoLado/2);
      v3.y = y0+calculaAltura(tamanhoLado);
      
      vertice = new ArrayList<>();
      vertice.add(v1);
      vertice.add(v2);
      vertice.add(v3);
        
        System.out.println("x0 = "+vertice.get(0).x+"   y0 = "+vertice.get(0).y);
        System.out.println("x1 = "+vertice.get(1).x+"   y1 = "+vertice.get(1).y);
        System.out.println("x2 = "+vertice.get(2).x+"   y2 = "+vertice.get(2).y);
    }
    
    private double calculaAltura(double tamanholado){
        double altura;
        altura = (tamanholado/2)*(Math.sqrt(3));
        return altura;
        }
    
}
